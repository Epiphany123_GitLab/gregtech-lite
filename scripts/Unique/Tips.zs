#priority -1

//  Heat Exchanger
<gregtech:machine:16052>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16052>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16052>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16052>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16052>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Extreme Exchanger
<gregtech:machine:16053>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16053>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16053>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16053>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16053>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Mega Exchanger
<gregtech:machine:16054>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16054>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16054>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16054>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16054>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Extreme Processing Array
<gregtech:machine:16057>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16057>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16057>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16057>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16057>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Ultimate Processing Array
<gregtech:machine:16058>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16058>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16058>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16058>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16058>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Mega Steam Turbine
<gregtech:machine:16060>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16060>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16060>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16060>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16060>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Mega Gas Turbine
<gregtech:machine:16061>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16061>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16061>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16061>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16061>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Mega Plasma Turbine
<gregtech:machine:16062>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16062>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16062>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16062>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16062>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  High Pressure Steam Turbine
<gregtech:machine:16063>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16063>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16063>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16063>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16063>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Supercritical Steam Turbine
<gregtech:machine:16064>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16064>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16064>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16064>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16064>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Mega High Pressure Steam Turbine
<gregtech:machine:16065>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16065>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16065>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16065>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16065>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Mega Supercritical Steam Turbine
<gregtech:machine:16066>.addTooltip(game.localize("crafttweaker.tips.test.bar"));
<gregtech:machine:16066>.addTooltip(game.localize("crafttweaker.tips.test.1"));
<gregtech:machine:16066>.addTooltip(game.localize("crafttweaker.tips.test.2"));
<gregtech:machine:16066>.addTooltip(game.localize("crafttweaker.tips.test.3"));
<gregtech:machine:16066>.addTooltip(game.localize("crafttweaker.tips.test.bar"));

//  Hermetic Casings
<gtlitecore:hermetic_casing>.addTooltip(game.localize("crafttweaker.tips.decorative"));
<gtlitecore:hermetic_casing:1>.addTooltip(game.localize("crafttweaker.tips.decorative"));
<gtlitecore:hermetic_casing:2>.addTooltip(game.localize("crafttweaker.tips.decorative"));
<gtlitecore:hermetic_casing:3>.addTooltip(game.localize("crafttweaker.tips.decorative"));
<gtlitecore:hermetic_casing:4>.addTooltip(game.localize("crafttweaker.tips.decorative"));