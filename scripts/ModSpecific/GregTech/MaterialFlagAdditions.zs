#loader gregtech
#priority 1000

import mods.gregtech.material.MaterialBuilder;
import mods.gregtech.material.Material;
import mods.gregtech.material.MaterialRegistry;

<material:redstone>.addFlags(["generate_gear"]);